#include <iostream>
#include <set>
#include <tuple>

using namespace std;


template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

class Gadget
{
private:
    int id_;
    string name_;

    static int id_generator_;
public:
    Gadget() : id_{++id_generator_}, name_{"unknown"}
    {}

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(const Gadget& source) : id_{source.id_}, name_{source.name_}
    {
        cout << "Gadget(cc: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(Gadget&& source) noexcept : id_{move(source.id_)}, name_{move(source.name_)}
    {
        cout << "Gadget(mv: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget& operator=(const Gadget& source)
    {
        if (this != &source)
        {
            id_ = source.id_;
            name_ = source.name_;

            cout << "op=(cc: " << id_ << ", " << name_ << ")" << endl;
        }

        return *this;
    }

    Gadget& operator=(Gadget&& source) noexcept
    {
        if (this != &source)
        {
            id_ = move(source.id_);
            name_ = move(source.name_);

            cout << "op=(mv: " << id_ << ", " << name_ << ")" << endl;
        }

        return *this;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    explicit operator int() const
    {
        return id_;
    }

    bool operator<(const Gadget& other) const
    {
        return id() < other.id();
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget(id: " << g.id() << ", name: " << g.name() << ")";

    return out;
}

struct CompareGadgetsById
{
    bool operator()(const Gadget& g1, const Gadget& g2) const
    {
        return g1.id() < g2.id();
    }
};

struct CompareGadgetsByName
{
    bool operator()(const Gadget& g1, const Gadget& g2) const
    {
        return g1.name() < g2.name();
    }
};

int main()
{
    set<int> numbers = { 6, 2, 6, 47, 234, 1, 4, 99, 4, 34, 32 };

    print(numbers, "numbers");

    numbers.insert(8);
    numbers.insert({7, 8, 9, 234, 455});

    print(numbers, "numbers");

    decltype(numbers.begin()) where;
    bool was_inserted;
    tie(where, was_inserted) = numbers.insert(97);

    if (was_inserted)
    {
        cout << "Wstawiono element " << *where << endl;
    }
    else
        cout << "Element " << *where << " byl juz w zbiorze" << endl;

    multiset<int> mset(numbers.begin(), numbers.end());

    mset.insert({6, 234, 454, 455, 97, 97, 234});

    print(mset, "mset");

    cout << "ilosc 234: " << mset.count(234) << endl;

    auto it = mset.find(88);

    if (it != mset.end())
        cout << "znalazlem element " << *it << endl;
    else
        cout << "Brak elementu " << 88 << endl;

    mset.erase(97);

    print(mset, "mset");

    decltype(mset.begin()) range_start, range_end;

    tie(range_start, range_end) = mset.equal_range(234);

    cout << "range: ";
    for(auto it = range_start; it != range_end; ++it)
        cout << *it << " ";
    cout << endl;

    tie(range_start, range_end) = mset.equal_range(55);

    if (range_start == range_end)
        mset.insert(range_start, 55);

    print(mset, "mset");

    set<int, greater<int>> numbers_desc(mset.begin(), mset.end());

    print(numbers_desc, "numbers_desc");

    set<Gadget, CompareGadgetsByName> stuff;

    stuff.emplace(1, "mp3");
    stuff.emplace(9, "ipad");
    stuff.emplace(5, "tv");
    stuff.emplace(4, "mobile");

    print(stuff, "stuff by id");

    auto compare_by_name_desc = [](const Gadget& g1, const Gadget& g2) { return g1.name() > g2.name();};

    set<Gadget, decltype(compare_by_name_desc)> stuff_by_name_desc(compare_by_name_desc);

    stuff_by_name_desc.insert(stuff.begin(), stuff.end());

    print(stuff_by_name_desc, "stuff_by_name_desc");
}