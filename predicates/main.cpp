#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>

using namespace std;

bool is_even(int x)
{
    return x % 2 == 0;
}

class IsBiggerThan //: public unary_function<int, bool>
{
    const int threshold_;
public:
    using argument_type = int;
    using result_type = bool;

    IsBiggerThan(int threshold) : threshold_{threshold}
    {}

    bool operator()(int x) const
    {
        return x > threshold_;
    }
};

class Lambda_23423423
{
    const int threshold_;
public:
    Lambda_23423423(int threshold) : threshold_{threshold}
    {}

    bool operator()(int x) const
    {
        return x < threshold_;
    }
};

void foo(function<void(int)> f)
{
    cout << "foo called!" << endl;
    f(100);
}

function<void(int)> binder(function<int (int, int)> f, int& value)
{
    return [f, &value](int x) { value += f(x, 10);};
}

int main()
{
    vector<int> numbers = { 1, 465, 234, 342, 124, 9, 534, 23, 2332, 23 };

    int threshold = 255;

    auto it = find_if(numbers.begin(), numbers.end(), IsBiggerThan(threshold));

    if (it != numbers.end())
    {
        cout << "item > " << threshold << " = " << *it << endl;
    }

    it = find_if(numbers.begin(), numbers.end(), not1(IsBiggerThan(threshold)));

    if (it != numbers.end())
        cout << "item <= " << threshold << " = " << *it << endl;

    vector<int> negate_numbers(numbers.size());

    transform(numbers.begin(), numbers.end(), negate_numbers.begin(), negate<int>());

    cout << "negate_numbers: ";
    for(const auto& item : negate_numbers)
        cout << item << " ";
    cout << endl;

    // adaptors

    it = find_if(numbers.begin(), numbers.end(), not1(ptr_fun(&is_even)));

    if (it != numbers.end())
        cout << "first odd: " << *it << endl;


    threshold = 123;

    it = find_if(numbers.begin(), numbers.end(), [=](int x) { return x < threshold; });


    int sum = 0;

    auto accumulator = [threshold, &sum](int x) { if (x < threshold) sum += x; };

    cout << "Lambda type: " << typeid(accumulator).name() << endl;


    for_each(numbers.begin(), numbers.end(),
             accumulator);

    foo(binder(plus<int>(), sum));

    cout << "sum: " << sum << endl;
}