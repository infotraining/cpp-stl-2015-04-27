#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <tuple>

using namespace std;

struct Aggregator
{
    int sum = 0;

    void operator()(int x)
    {
        cout << "Aggregator(" << x << ")\n";
        sum += x;
    }
};

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

int main()
{
    vector<int> vec = { 434, 23, 534, 12, 55, 2, 3, 5, 2, 2, 5, 512, 124, 99 };

    cout << "count(2) = " << count(vec.begin(), vec.end(), 2) << endl;

    Aggregator agg = for_each(vec.begin(), vec.end(), Aggregator());

    cout << "sum: " << agg.sum << endl;

    decltype(vec)::iterator min_it, max_it;
    tie(min_it, max_it) = minmax_element(vec.begin(), vec.end());

    cout << "min = " << *min_it << "; max = " << *max_it << endl;

    auto first_duplication = adjacent_find(vec.begin(), vec.end());

    if (first_duplication != vec.end())
        cout << "duplication: " << *first_duplication << endl;

    auto wanted = { 13, 512, 1024, 2048 };

    auto first_wanted = find_first_of(vec.begin(), vec.end(), wanted.begin(), wanted.end());

    if (first_wanted != vec.end())
        cout << "first wanted: " << *first_wanted << endl;

    vector<int> numbers = { 6, 8, 10, 10, 10, 77, 124, 245, 63456, 98989 };

    cout << "is_sorted: " << is_sorted(numbers.begin(), numbers.end()) << endl;

    vector<int>::iterator range_start, range_end;

    tie(range_start, range_end) = equal_range(numbers.begin(), numbers.end(), 10);

    for(auto it = range_start; it != range_end; ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    cout << "\n\nRemove:\n";

    print(vec, "vec before");

    auto garbage_start = remove_if(vec.begin(), vec.end(), [](int x) { return x > 255;});

    vec.erase(garbage_start, vec.end());

    print(vec, "vec after");
}