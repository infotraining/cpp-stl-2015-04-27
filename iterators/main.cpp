#include <iostream>
#include <algorithm>
#include <list>
#include <deque>
#include <iterator>

using namespace std;

template <typename InputIter, typename T>
InputIter mfind(InputIter first, InputIter last, const T& value)
{
    auto it = first;
    while (it != last)
    {
        if (*it == value)
            break;

        ++it;
    }

    return it;
}

template <typename InputIter, typename OutputIter>
OutputIter mcopy(InputIter first, InputIter last, OutputIter dest)
{
    for(auto it = first; it != last; ++it)
    {
        *dest = *it;
        ++dest;
    }

    return dest;
}


namespace Before
{
    template <typename InputIter, typename OutputIter>
    OutputIter mcopy_if(InputIter first, InputIter last, OutputIter dest,
                        bool (* predicate)(typename iterator_traits<InputIter>::value_type))
    {
        for (auto it = first; it != last; ++it)
        {
            if (predicate(*it))
            {
                *dest = *it;
                ++dest;
            }
        }

        return dest;
    }
}

template <typename InputIter, typename OutputIter, typename Predicate>
OutputIter mcopy_if(InputIter first, InputIter last, OutputIter dest,
                    Predicate predicate)
{
    for (auto it = first; it != last; ++it)
    {
        if (predicate(*it))
        {
            *dest = *it;
            ++dest;
        }
    }

    return dest;
}

template <typename T>
using Container = list<T>;

bool is_even(int x)
{
    return x % 2 == 0;
}

int main()
{
    int tab[] = { 1, 2, 3, 4, 5, 7, 812, 33 };

    vector<int>::iterator::value_type x;

    auto it = mfind(begin(tab), end(tab), 7);

    if (it != end(tab))
        cout << "I've found: " << *it << endl;
    else
        cout << "Item not found." << endl;

    vector<int> lst;

    mcopy_if(begin(tab), end(tab), back_inserter(lst), [](int x) { return x % 2 == 0; });

    cout << "lst: ";
    for(const auto& item : lst)
        cout << item << " ";
    cout << endl;

    // reverse itearators

    Container<int> container = { 1, 6, 23, 34, 65, 345, 23, 123, 5, 6};

    cout << "container: ";
    for(auto it = container.cbegin(); it != container.cend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    cout << "reversed: ";
    for(auto rit = container.crbegin(); rit != container.crend(); ++rit)
    {
        cout << *rit << " ";
    }
    cout << endl;

    auto left_item = mfind(container.begin(), container.end(), 23);

    if (left_item != container.end())
        cout << "left item: " << *left_item << endl;

    auto right_item = mfind(container.rbegin(), container.rend(), 23);

    if (right_item != container.rend())
        cout << "right item: " << *right_item << endl;

    cout << "range from left to right: ";
    for(auto it = left_item; it != right_item.base(); ++it)
        cout << *it << " ";
    cout << endl;

    cout << "range from right to left: ";
    for(auto it = right_item; it != Container<int>::reverse_iterator(left_item); ++it)
        cout << *it << " ";
    cout << endl;

    // advance + distance

    auto kroczer = container.begin();

    advance(kroczer, 3);

    cout << "*kroczer: " << *kroczer << endl;

    cout << "distance(begin, kroczer): " <<  distance(container.begin(), kroczer) << endl;

    // stream iterators
    cout << "Enter numbers:" << endl;

    istream_iterator<int> start(cin);
    istream_iterator<int> end;

    vector<int> numbers(start, end);

    cout << "\n\nNumbers: ";
    mcopy(numbers.begin(), numbers.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}