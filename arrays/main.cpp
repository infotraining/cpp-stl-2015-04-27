#include <iostream>
#include <array>

using namespace std;

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }

    cout << "]" << endl;
}

void legacy_code(int* tab, size_t size)
{
    for(int* it = tab; it != tab + size; ++it)
        cout << "item: " << *it << endl;
}

class Test
{
    array<int, 10> test_data_ = { { 1, 2, 3, 4, 5 } };
};

struct X
{
    int tab[10];
    int value;
};

X x = { { 1, 2, 3 }, 3 };

int main()
{
    int tab[10] = {1, 2, 3, 4, 5, 6, 7};

    print(tab, "tab");

    array<int, 10> arr1 = { 1, 2, 3, 4, 7 };

    print(arr1, "arr1");

    for(auto rit = arr1.crbegin(); rit != arr1.crend(); ++rit)
        cout << *rit << " ";
    cout << endl;

    cout << "arr1.size = " << arr1.size() << endl;

    array<int, 10> arr2 = { 2, 45, 6, 76, 7, 234 };

    print(arr1, "arr1 before swap");
    print(arr2, "arr2 before swap");

    arr1.swap(arr2);

    print(arr1, "arr1 after swap");
    print(arr2, "arr2 after swap");

    legacy_code(arr2.data(), arr2.size());

    array<int, 0> zero_arr;

    print(zero_arr, "zero_arr");
}