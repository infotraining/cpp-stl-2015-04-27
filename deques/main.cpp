#include <iostream>
#include <deque>

using namespace std;

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

class Gadget
{
private:
    int id_;
    string name_;

    static int id_generator_;
public:
    Gadget() : id_{++id_generator_}, name_{"unknown"}
    {}

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(const Gadget& source) : id_{source.id_}, name_{source.name_}
    {
        cout << "Gadget(cc: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(Gadget&& source) noexcept : id_{move(source.id_)}, name_{move(source.name_)}
    {
        cout << "Gadget(mv: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget& operator=(const Gadget& source)
    {
        if (this != &source)
        {
            id_ = source.id_;
            name_ = source.name_;

            cout << "op=(cc: " << id_ << ", " << name_ << ")" << endl;
        }

        return *this;
    }

    Gadget& operator=(Gadget&& source) noexcept
    {
        if (this != &source)
        {
            id_ = move(source.id_);
            name_ = move(source.name_);

            cout << "op=(mv: " << id_ << ", " << name_ << ")" << endl;
        }

        return *this;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    explicit operator int() const
    {
        return id_;
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget(id: " << g.id() << ", name: " << g.name() << ")";

    return out;
}

int main()
{
    deque<Gadget> stuff;

    Gadget g{1, "mp3"};
    stuff.push_back(g);
    stuff.emplace_back(2, "ipad");
    stuff.emplace_front(3, "mobile");

    stuff.push_front(Gadget{4, "tv"});

    print(stuff, "stuff");

    Gadget& my_favorite = stuff[2];

    cout << "my_favorite: " << my_favorite << endl;

    stuff.emplace_front(5, "camera");

    print(stuff, "stuff after emplace_front");

    cout << "my_favorite: " << my_favorite << endl;

    return 0;
}