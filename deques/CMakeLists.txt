cmake_minimum_required(VERSION 3.1)
project(deques)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp)
add_definitions("-Wall -pedantic")
add_executable(deques ${SOURCE_FILES})