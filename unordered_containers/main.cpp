#include <iostream>
#include <unordered_set>
#include <tuple>
#include <boost/functional/hash.hpp>

using namespace std;

class Book
{
    int id_;
    string isbn_;
public:
    Book(int id, const string& isbn) : id_{id}, isbn_{isbn}
    {};

    int id() const
    {
        return id_;
    }

    string isbn() const
    {
        return isbn_;
    }

    bool operator==(const Book& other) const
    {
        return tie(id_, isbn_) == tie(other.id_, other.isbn_);
    }
};

namespace std
{
    template <>
    struct hash<Book>
    {
        size_t operator()(const Book& b) const
        {
            size_t seed = 0;

            boost::hash_combine(seed, b.id());
            boost::hash_combine(seed, b.isbn());

            return seed;
        }
    };
}

struct BookHasher
{
    size_t operator()(const Book& b) const
    {
        size_t seed = 0;

        boost::hash_combine(seed, b.id());
        boost::hash_combine(seed, b.isbn());

        return seed;
    }
};

int main()
{
    unordered_set<Book, BookHasher> books =
        { Book{1, "SJFSFS7E3"}, Book{4, "FSDFSDF785"},
          Book{3, "623784JJK"}, Book{32, "LKJLKJ767"} };

    for(auto index = 0u; index < books.bucket_count(); ++index)
    {
        cout << "bucket[" << index << "] = " << books.bucket_size(index) << endl;

    }
}