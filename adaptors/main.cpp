#include <iostream>
#include <stack>
#include <queue>

using namespace std;

int main()
{
    stack<string, vector<string>> lifo_words;

    lifo_words.push("one");
    lifo_words.push("two");
    lifo_words.push("three");

    while (!lifo_words.empty())
    {
        auto item = lifo_words.top();

        cout << "item: " << item << endl;

        lifo_words.pop();
    }

    cout << "\n\n";

    queue<string> fifo;

    fifo.push("one");
    fifo.push("two");
    fifo.push("three");

    while(!fifo.empty())
    {
        string item = fifo.front();

        cout << "item: " << item << endl;

        fifo.pop();
    }

    cout << "\n\n";

    priority_queue<string, vector<string>, greater<string>> pq;

    pq.push("one");
    pq.push("two");
    pq.push("three");

    while(!pq.empty())
    {
        string item = pq.top();

        cout << "item: " << item << endl;

        pq.pop();
    }

    return 0;
}