#include <iostream>
#include <map>

using namespace std;

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "{" << p.first << ", " << p.second << "}";
    return out;
}

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }
    cout << "]" << endl;
}

int main()
{
    map<int, string> numbers = { {1, "one"}, {3, "three"}, {2, "two"} };

    print(numbers, "numbers");

    numbers.insert(map<int, string>::value_type(6, "six"));
    numbers.insert(decltype(numbers)::value_type(5, "five"));
    numbers.insert(pair<const int, string>(8, "eight"));
    numbers.insert(make_pair(10, "ten"));

    print(numbers, "numbers");

    auto it_key = numbers.find(8);
    if (it_key != numbers.end())
    {
        cout << it_key->second << " has been found." << endl;
    }
    else
        cout << "Key not found." << endl;

    try
    {
        cout << "16: " << numbers.at(16) << endl;
    }
    catch(const out_of_range& e)
    {
        cout << "Key not found. " << e.what() << endl;
    }

    print(numbers, "numbers");

    multimap<int, string, greater<int>> numbers_desc(numbers.begin(), numbers.end());

    numbers_desc.insert(make_pair(9, "neuf"));
    numbers_desc.insert(make_pair(9, "dziewiec"));
    numbers_desc.insert(make_pair(9, "nine"));

    print(numbers_desc, "numbers_desc");

    decltype(numbers_desc)::iterator range_start, range_end;

    tie(range_start, range_end) = numbers_desc.equal_range(9);

    cout << "9: ";
    for(auto it = range_start; it != range_end; ++it)
        cout << it->second << " ";
    cout << endl;
    return 0;
}