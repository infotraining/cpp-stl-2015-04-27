#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

template <typename T>
void describe_vec(const vector<T>& vec, const string& description)
{
    cout << description << " - ";
    cout << "size: " << vec.size()
         << "; capacity: " << vec.capacity() << endl;
}

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
    {
        cout << item << " ";
    }

    // 1.
//    for(typename Container::const_iterator it = container.begin(); it != container.end(); ++it)
//    {
//        const typename Container::value_type& item = *it;
//
//        cout << item << " ";
//    }

    // 2.
//    for(const auto& it = begin(container); it != end(container); ++it)
//    {
//        const auto& item = *it;
//
//        cout << item << " ";
//    }

    cout << "]" << endl;
}

class Gadget
{
    int id_;
    string name_;

    static int id_generator_;
public:
    Gadget() : id_{++id_generator_}, name_{"unknown"}
    {}

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {}

    Gadget(const Gadget& source) : id_{source.id_}, name_{source.name_}
    {
        cout << "Gadget(cc: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget(Gadget&& source) noexcept : id_{move(source.id_)}, name_{move(source.name_)}
    {
        cout << "Gadget(mv: " << id_ << ", " << name_ << ")" << endl;
    }

    Gadget& operator=(const Gadget& source)
    {
        if (this != &source)
        {
            id_ = source.id_;
            name_ = source.name_;

            cout << "op=(cc: " << id_ << ", " << name_ << ")" << endl;
        }

        return *this;
    }

    Gadget& operator=(Gadget&& source) noexcept
    {
        if (this != &source)
        {
            id_ = move(source.id_);
            name_ = move(source.name_);

            cout << "op=(mv: " << id_ << ", " << name_ << ")" << endl;
        }

        return *this;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    explicit operator int() const
    {
        return id_;
    }
};

ostream& operator<<(ostream& out, const Gadget& g)
{
    out << "Gadget(id: " << g.id() << ", name: " << g.name() << ")";

    return out;
}

int Gadget::id_generator_ = 0;

namespace UniversalReference
{
    template <typename T>
    void foo(T&& t)
    {

    }

    // T v1;
    // foo(v1);   // foo(T&) // template <typename T> void foo(T& && t)
    // foo(T());  // foo(T&&)
    // auto&& a1 = v1;  // T& a1 = v1;
    // auto&& a2 = T(); // T&& a2 = T();
}

int main()
{
    vector<int> vec1;
    describe_vec(vec1, "vec1");
    print(vec1, "vec1");

    vector<int> vec2(10);
    describe_vec(vec2, "vec2");
    print(vec2, "vec2");

    vector<int> vec3(10, -1);
    print(vec3, "vec3");

    vector<Gadget> gadgets(5);
    print(gadgets, "gadgets");

    vector<Gadget> gadgets2(5, Gadget{7, "mp3 player"});
    print(gadgets2, "gadgets2");

    int tab[] = { 1, 2, 3, 4 };
    vector<int> vec4(begin(tab), end(tab) - 1);
    print(vec4, "vec4");

    vector<int> vec5 = { static_cast<int>(Gadget{8, "laptop"}), 1, 2, 3, 4, 5, 6, 7 };
    print(vec5, "vec5");

    vector<string> words1{5, "one"};
    print(words1, "words1");

    vector<string> words2(words1);
    print(words2, "words2");

    vector<string> words3 = move(words1);
    print(words3, "words3 after move");
    print(words1, "words1 after move");
    describe_vec(words1, "words1 after move");

    vector<string> items(10);

    items[0] = "one"; // ub

    try
    {
        items.at(12) = "twelve";
    }
    catch(const out_of_range& e)
    {
        cout << "error: " << e.what() << endl;
    }

    cout << "\n\nreversed iterators:\n";

    const vector<int> numbers = { 1, 2, 3, 4, 5, 7, 8, 9, 10 };

    cout << "numbers in reversed orders: ";
    for(auto rit = numbers.rbegin(); rit != numbers.rend(); ++rit)
    {
        cout << *rit << " ";
    }
    cout << endl;

    cout << "\n\nresizing:\n";

    vector<int> resizeable_vec;

    resizeable_vec.reserve(127);

    for(int i = 0; i < 128; ++i)
    {
        resizeable_vec.push_back(i);
        describe_vec(resizeable_vec, "resizeable");
    }

    //resizeable_vec.clear();
    resizeable_vec.resize(10);
    resizeable_vec.shrink_to_fit();
    //vector<int>(resizeable_vec).swap(resizeable_vec);

    describe_vec(resizeable_vec, "resizeable_vec after clear()");

    cout << "\n\npush_back:\n";

    vector<Gadget> stuff;

    Gadget mp3{1, "mp3"};
    stuff.push_back(move(mp3));
    stuff.push_back(Gadget(2, "ipad"));
    stuff.push_back(Gadget(3, "laptop"));
    stuff.emplace_back(4, "tv");

    cout << "\n\ninsert:" << endl;

    auto where = stuff.begin() + 2;

    //stuff.insert(where, Gadget{13, "mobile"});
    stuff.emplace(where, 13, "mobile");
    print(stuff, "stuff");

    Gadget* ptr_gadget = stuff.data(); // &stuff[0]
    ptr_gadget++;

    cout << *ptr_gadget << endl;

    cout << "\n\nvector<bool>:\n";

    vector<bool> flags(10);

    print(flags, "flags");

    flags[3] = true;

    flags.back().flip();

    print(flags, "flags");

    flags.flip();

    print(flags, "flags after flip");

    for(auto&& bit : flags) // auto&& - universal reference
    {
        bit.flip();
    }
};