#include <vector>
#include <random>
#include <set>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix)
{
    cout << prefix << " : [ ";
    for(const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    vector<int> vec(35);

    std::random_device rd;
    std::mt19937 mt {0};
    std::uniform_int_distribution<int> uniform_dist {1, 35};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    vector<int> sorted_vec {vec};
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset {vec.begin(), vec.end()};

    print(vec, "vec: ");
    print(sorted_vec, "sorted_vec: ");
    print(mset, "mset: ");

    // czy 17 znajduje sie w sekwencji?
    cout << "\n\nhas 17:\n";
    cout << "----------------\n";
    cout << "vec: ";
    cout << (find(vec.begin(), vec.end(), 17) != vec.end());

    cout << "\nsorted_vec:";
    cout << binary_search(sorted_vec.begin(), sorted_vec.end(), 17);

    cout << "\nmset: ";
    cout << (mset.find(17) != mset.end()) << endl;

    // ile razy wystepuje 22?
    // czy 17 znajduje sie w sekwencji?
    cout << "\n\ncount 22:\n";
    cout << "----------------\n";
    cout << "vec: ";
    cout << count(vec.begin(), vec.end(), 22);

    cout << "\nsorted_vec: ";
    vector<int>::iterator range_start, range_end;
    tie(range_start, range_end) = equal_range(sorted_vec.begin(), sorted_vec.end(), 22);
    cout << distance(range_start, range_end);

    cout << "\nmset: ";
    cout << mset.count(22);

    // usun wszystkie wieksze od 15
    cout << "\n\nremoving > 15:\n";
    cout << "----------------\n";
    vec.erase(remove_if(vec.begin(), vec.end(), [](int x) { return x > 15; }), vec.end());
    print(vec, "vec");

    sorted_vec.erase(upper_bound(sorted_vec.begin(), sorted_vec.end(), 15), sorted_vec.end());
    print(sorted_vec, "sorted_vec");

    mset.erase(mset.upper_bound(15), mset.end());
    print(mset, "mset");
}
