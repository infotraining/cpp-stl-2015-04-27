#include <map>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <boost/tokenizer.hpp>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym.
    Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

vector<string> load_words(ifstream& in)
{
    vector<string> words;

    while (in)
    {
        string word;

        in >> word;
        words.push_back(word);
    }

    return words;
}

unordered_map<string, int> make_concordance(vector<string>& words)
{
    unordered_map<string, int> concordance;

    for(const auto& word : words)
        concordance[word]++;

    return concordance;
}

int main()
{
    ifstream in("holmes.words");

    if (!in)
    {
        cout << "File not found." << endl;
        throw runtime_error("File not found.");
    }

    vector<string> words = load_words(in);

    auto time_start = chrono::high_resolution_clock::now();

    auto concordance = make_concordance(words);

//    multimap<int, string, greater<int>> freq_words;
    vector<decltype(concordance)::iterator> freq_words;
    freq_words.reserve(concordance.size());

//    for(const auto& kv : concordance)
//        freq_words.insert(make_pair(kv.second, kv.first));

    for(auto it = concordance.begin(); it != concordance.end(); ++it)
        freq_words.push_back(it);

    using Iter = decltype(concordance)::iterator;
    partial_sort(freq_words.begin(), freq_words.begin() + 10, freq_words.end(),
                 [](const Iter& i1, const Iter& i2) { return i1->second > i2->second; });

    auto time_end = chrono::high_resolution_clock::now();

    cout << "No of words: " << words.size() << endl;

    cout << "\nTop 10: \n";

//    int counter = 0;
//    for(const auto& kv : freq_words)
//    {
//        ++counter;
//        cout << kv.second << " - " << kv.first << endl;
//
//        if (counter == 10)
//            break;
//    }

    int counter = 0;
    for(const auto& iter : freq_words)
    {
        ++counter;
        cout << iter->first << " - " << iter->second << endl;

        if (counter == 10)
            break;
    }

    cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(time_end - time_start).count() << "ms" << endl;
}




