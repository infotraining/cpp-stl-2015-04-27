#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <chrono>

using namespace std;

/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

// szablony funkcji umożliwiające sortowanie kontenera
template <typename Container>
void sort(Container& container)
{
    sort(container.begin(), container.end());
}

template <typename T>
void sort(list<T>& lst)
{
    lst.sort();
}

template <typename T>
using Container = vector<T>;

int main()
{
    const int n = 10000000;

    // utwórz kontener

    Container<int> container;
    container.reserve(n);


    {
        cout << "Preparing data... Filling container." << endl;

        mt19937 gen(0);
        uniform_int_distribution<> random;

        auto t1 = chrono::high_resolution_clock::now();

        // wypełnij kontener danymi
        for(int i = 0; i < n; ++i)
            container.push_back(random(gen));

        auto t2 =  chrono::high_resolution_clock::now();

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << "ms" << endl;
    }

    {
        cout << "\nStart sorting..." << endl;

        auto t1 = chrono::high_resolution_clock::now();

        // posortuj kontener
        sort(container);

        auto t2 =  chrono::high_resolution_clock::now();

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << "ms" << endl;
    }
}
