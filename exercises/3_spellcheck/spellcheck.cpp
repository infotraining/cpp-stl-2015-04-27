#include <iostream>
#include <fstream>
#include <set>
#include <unordered_set>
#include <string>
#include <chrono>
#include <vector>
#include <boost/tokenizer.hpp>

using namespace std;

template <typename UnorderedSet>
void describe_uset(const UnorderedSet& uset)
{
    cout << "size = " << uset.size() << "; bucket_count = " << uset.bucket_count()
         << "; load_factor = " << uset.load_factor()
         << "; max_load_factor = " << uset.max_load_factor() << endl;
}

int main()
{
     // wszytaj zawartość pliku en.dict ("słownik języka angielskiego")
     // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list = { "this",  "is", "an",  "exmple", "of", "very", "badd", "snetence" };

    for(const auto& word : words_list)
        cout << word << ";";
    cout << endl;

    ifstream in("en.dict");

    if (!in)
    {
        cout << "Error! File is not opened..." << endl;
        exit(1);
    }

    istream_iterator<string> start(in);
    istream_iterator<string> end;

    unordered_set<string> dictionary_en(start, end);

//    describe_uset(dictionary_en);
//
//    auto prev_bucket_count = dictionary_en.bucket_count();
//
//    while (in)
//    {
//        string word;
//        in >> word;
//
//        dictionary_en.insert(word);
//
//        auto current_bucket_count = dictionary_en.bucket_count();
//        if (prev_bucket_count != current_bucket_count)
//        {
//            cout << "rehashed; ";
//            describe_uset(dictionary_en);
//            prev_bucket_count = current_bucket_count;
//        }
//    }

    cout << "after insert: ";
    describe_uset(dictionary_en);

    dictionary_en.max_load_factor(0.5);
    cout << "after rehash: ";
    describe_uset(dictionary_en);

    cout << "size of dict: " << dictionary_en.size() << endl;

    vector<string> misspelled_words;

    for(const auto& word : words_list)
    {
        auto it = dictionary_en.find(word);

        if (it == dictionary_en.end())
            misspelled_words.push_back(word);
    }

    cout << "\nErrors: ";
    for(const auto& word : misspelled_words)
        cout << word << " ";
    cout << endl;
};

